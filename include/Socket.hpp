//
//  Socket.hpp
//  SocketServer
//
//  Created by Matthew Cross on 09/03/2017.
//
//

#ifndef Socket_hpp
#define Socket_hpp

#include <string>
#include <stdio.h>
#include <netinet/in.h>

namespace mc
{
	namespace networking
	{
		class socket
		{
		private:
			int m_fd;
			
		public:
			explicit socket(int fd);
			virtual ~socket();
			
			int get_file_descriptor() const { return m_fd; }
			
			void close();
			
			virtual ssize_t send_bytes(const void *buf, size_t len) const;
			virtual ssize_t read_bytes(void *buf, size_t len) const;
		};
		
		class internet_socket : public socket
		{
		private:
			struct sockaddr_in m_addr;
			std::string m_ip;
			int m_port;
			
		public:
			internet_socket();
			explicit internet_socket(int fd);
			
			void set_address(const struct sockaddr_in& addr);
			struct sockaddr_in& get_address() noexcept;
			
			const std::string& get_ip_address() const noexcept;
			int get_port() const noexcept;
		};
	}
}

#endif /* Socket_hpp */
