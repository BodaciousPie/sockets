//
//  SocketServer.hpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#ifndef SocketServer_hpp
#define SocketServer_hpp

#include <iostream>

#include <memory>

#include "Socket.hpp"

namespace mc
{
	namespace networking
	{
		class socket_server
		{
		private:
			std::unique_ptr<socket> m_pListeningSocket;
			struct sockaddr_in m_serverAddress;
			
			const int m_maxConnectionQueueLength = 5;
			
		public:
			explicit socket_server(std::unique_ptr<socket> upSocket);
			socket_server();
			
			void bind_port(const uint16_t port);
			void listen();
			
			void accept(const std::function<void(socket& peerSocket)>& handlerFunc);
			
			virtual ~socket_server();
			
		private:
			int get_bound_port() const noexcept;
		};
	}
}

#endif /* SocketServer_hpp */
