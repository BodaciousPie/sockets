//
//  SocketClient.hpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#ifndef SocketClient_hpp
#define SocketClient_hpp

#include <iostream>
#include <string>
#include <netdb.h>
#include <sys/types.h>
#include <memory>
#include "Socket.hpp"

namespace mc
{
	namespace networking
	{
		class socket_client
		{
		private:
			std::unique_ptr<socket> m_upSocket;
			struct hostent *m_pServer = nullptr;
			
		public:
			explicit socket_client(std::unique_ptr<socket> upSocket);
			socket_client();
			
			socket& get_socket() const { return *m_upSocket; };
			
			virtual ~socket_client();
			
			virtual void connect_to_server(const std::string& host, const uint16_t port);
		};
	}
}

#endif /* SocketClient_hpp */
