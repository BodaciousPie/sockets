//
//  ChatRoom.cpp
//  Sockets
//
//  Created by Matthew Cross on 07/04/2018.
//
//

#include <assert.h>
#include <poll.h>
#include <mutex>
#include <vector>
#include <sstream>
#include <algorithm>
#include <fstream>
#include <numeric>
#include <iterator>
#include <memory.h>

#include "ChatRoom.hpp"

//#define LOG_FILE

namespace
{
    size_t g_participant_id = 0;
	std::mutex g_log_file_mutex;
	
	void log_message(const std::string& msg)
	{
		std::cout << msg << std::endl;
		
//todo - move logging to parent class
#ifdef LOG_FILE
		std::lock_guard<std::mutex> participant_mutex_lock(g_log_file_mutex);
		
		std::ofstream g_log_file;
		
		if (!g_log_file.is_open())
		{
			g_log_file.open("server_log.txt", std::ofstream::out | std::ofstream::app);
		}
		else
		{
			std::cerr << "Failed to open log file." << std::endl;
		}
		
		if (g_log_file.is_open()) g_log_file << msg << std::endl;
#endif
	}
	
	std::mutex g_participants_mutex;
	
	ssize_t read_message(std::string& out, mc::networking::socket& socket)
	{
		char c = -1;
		ssize_t rv = 0;
		
		while (true)
		{
			c = -1;
			
			ssize_t bytes_read = 0;
			
			if ((bytes_read = socket.read_bytes(&c, 1)) > 0)
			{
				rv += bytes_read;
				
				if ((c == '\0') || (c == EOF)) break;
				else out += c;
			}
			else
			{
				break;
			}
		}
		
		return rv;
	}
	
	void trim(std::string& out)
	{
		size_t whitespace_count = 0;
		
		for (auto rit = out.rbegin(); rit != out.rend(); ++rit)
		{
			if (std::isspace(*rit)) ++whitespace_count;
			else break;
		}
		
		out = out.substr(0, out.length() - whitespace_count);
	}
	
	void tokenise(std::vector<std::string>& out, const std::string& str)
	{
		std::istringstream iss(str);
		out.assign((std::istream_iterator<std::string>(iss)), std::istream_iterator<std::string>());
	}

    size_t next_participant_id()
    {
        std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);

        return ++g_participant_id;
    }
}

mc::chat::chat_room::chat_room()
:
mc::networking::socket_server(),
m_participants(),
m_max_size(100),
m_room_name(),
m_port(8000)
{
	
}

void mc::chat::chat_room::set_port(const uint16_t port)
{
	m_port = port;
}

void mc::chat::chat_room::set_max_room_size(size_t max) noexcept
{
	m_max_size = max;
}

size_t mc::chat::chat_room::get_max_room_size() const noexcept
{
	return m_max_size;
}

void mc::chat::chat_room::set_room_name(const std::string& name) noexcept
{
	m_room_name = name;
}

const std::string& mc::chat::chat_room::get_room_name() const noexcept
{
	return m_room_name;
}

void mc::chat::chat_room::add_participant(std::shared_ptr<participant>& pParticipant)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	assert(m_participants.count(pParticipant->m_user_id) == 0);
	m_participants[pParticipant->m_user_id] = pParticipant;
	
	log_message("Added participant: " + std::to_string(pParticipant->m_user_id));
}

void mc::chat::chat_room::remove_participant(int pid)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	if (m_participants.count(pid) > 0)
	{
		m_participants.erase(pid);
		
		log_message("Removed participant: " + std::to_string(pid));
	}
}

//Message participant from the server
void mc::chat::chat_room::message_participant(int participant_id, const std::string& message)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	const std::string full_msg("[SERVER]: " + message);
	log_message(full_msg);
	write_socket_msg(m_participants.at(participant_id)->m_socket, full_msg);
}

void mc::chat::chat_room::message_all(const std::string& msg)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	const std::string full_msg("[SERVER]: " + msg);
	
	log_message(full_msg);
	
	for (auto const& kvp : m_participants)
	{
		networking::internet_socket& socket = kvp.second->m_socket;
		
		write_socket_msg(socket, full_msg);
	}
}

void mc::chat::chat_room::message_from_sender_to_all(const participant& sender, const std::string& msg)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	const std::string full_msg("[" + sender.m_username + "]: " + msg);
	
	log_message(full_msg);
	
	for (auto const& kvp : m_participants)
	{
		int pid = kvp.first;
		
		if (pid != sender.m_user_id)
		{
			networking::internet_socket& socket = kvp.second->m_socket;
			
			write_socket_msg(socket, full_msg);
		}
	}
}

void mc::chat::chat_room::write_socket_msg(const networking::socket& socket, const std::string& msg) const
{
	const std::string formatted(msg + "\n");
	socket.send_bytes(formatted.c_str(), formatted.length() + 1);
}

void mc::chat::chat_room::open_room()
{
	bind_port(m_port);
	listen();
	accept([this](networking::socket &peerSocket){
		if ((m_participants.size() + 1) > m_max_size)
		{
			peerSocket.close();//room full
		}
		else
		{
			std::shared_ptr<participant> pParticipant = std::make_shared<participant>();
            pParticipant->m_user_id		= next_participant_id();
			pParticipant->m_username	= "User_" + std::to_string(pParticipant->m_user_id);
			pParticipant->m_socket		= static_cast<networking::internet_socket&>(peerSocket);
			
			add_participant(pParticipant);
			
			handle_new_participant(*pParticipant);
		}
	});
}

void mc::chat::chat_room::close_room()
{
	
}

void mc::chat::chat_room::handle_new_participant(participant& p)
{
	send_participant_joined_message(p);
	send_welcome_message(p);
	send_user_commands(p);
	handle_list_users_command(p);
	
	while (true)
	{
		struct pollfd pfd;
		memset(&pfd, 0, sizeof(struct pollfd));
		pfd.fd		= p.m_socket.get_file_descriptor();
		pfd.events	= POLLIN | POLLERR | POLLHUP | POLLNVAL;
		
		if (poll(&pfd, 1, -1) == -1) break;
		
		if ((pfd.revents & POLLIN) == 1)
		{
			std::string msg;
			
			//todo strip newlines
			if (read_message(msg, p.m_socket) > 0)
			{
				trim(msg);
				
				if (msg.substr(0, 2) == "//")
				{
					if (msg.find("//set_name") == 0)
					{
						handle_set_name_command(p, msg);
					}
					else if (msg == "//list_users")
					{
						handle_list_users_command(p);
					}
					else
					{
						//unknown command
					}
				}
				else if (!msg.empty())
				{
					message_from_sender_to_all(p, msg);
				}
			}
			else
			{
				log_message("Participant \"" + p.m_username + "\" shutdown.");
				break;
			}
		}
		else if (((pfd.revents & POLLERR) == 1) || ((pfd.revents & POLLHUP) == 1) || ((pfd.revents & POLLNVAL) == 1))
		{
			log_message("poll failed - removing user \"" + p.m_username + "\".");
			break;
		}
	}
	
	remove_participant(p.m_user_id);
	message_all(p.m_username + " has left the room.");
}

void mc::chat::chat_room::send_participant_joined_message(const participant& p)
{
	message_all(p.m_username + " has entered the room.");
}

void mc::chat::chat_room::send_welcome_message(const participant& p)
{
	message_participant(p.m_user_id, "Welcome to " + m_room_name + ".");
}

void mc::chat::chat_room::send_user_commands(const participant& p)
{
	message_participant(p.m_user_id, "Usage:\n\t//set_name [ARGUMENT]\n\t//list_users");
}

void mc::chat::chat_room::handle_set_name_command(participant& p, const std::string& command)
{
	std::vector<std::string> tokens;
	tokenise(tokens, command);
	
	if (tokens.size() > 1)
	{
		std::string new_name;
		
		std::vector<std::string>::iterator it = tokens.begin();
		std::advance(it, 2);
		
		new_name = std::accumulate(it, tokens.end(), tokens[1], [](const std::string& a, const std::string& b) {
			return a + ' ' + b;
		});
		
		if (new_name == p.m_username)
		{
			message_participant(p.m_user_id, "The set_name command failed as your username is already \"" + new_name + "\".");
		}
		else if (!search_for_participant_by_name(new_name))
		{
			const std::string full_msg("Participant \"" + p.m_username + "\" set their name to \"" + new_name + "\".");
			
			message_all(full_msg);
			
			p.m_username = new_name;
		}
		else
		{
			message_participant(p.m_user_id, "The set_name command failed as the user name \"" + new_name + "\" is taken.");
		}
	}
	else
	{
		message_participant(p.m_user_id, "The set_name command requires an argument.");
	}
}

bool mc::chat::chat_room::search_for_participant_by_name(const std::string& name, int *pParticipant_id /*= nullptr*/)
{
	std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
	
	auto it = std::find_if(m_participants.begin(), m_participants.end(), [&name](const int_participant_map_t::value_type& kvp) {
		const participant& p = *kvp.second;
		
		return p.m_username == name;
	});
	
	bool found = (it != m_participants.cend());
	
	if (pParticipant_id) *pParticipant_id = found ? it->second->m_user_id : -1;
	
	return found;
}

void mc::chat::chat_room::handle_list_users_command(const participant& p)
{
	std::string list;
	
	{
		std::lock_guard<std::mutex> participant_mutex_lock(g_participants_mutex);
		
		std::stringstream ss;
		
		ss << "Currently connected participants: ";
		
		for (auto it = m_participants.begin(); it != m_participants.end();)
		{
			ss << it->second->m_username;
			
			if (++it == m_participants.cend()) break;
			
			ss << ", ";
		}
		
		list = ss.str();
	}
	
	message_participant(p.m_user_id, list);
}
