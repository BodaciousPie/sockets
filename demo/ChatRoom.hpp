//
//  ChatRoom.hpp
//  Sockets
//
//  Created by Matthew Cross on 07/04/2018.
//
//

#ifndef ChatRoom_hpp
#define ChatRoom_hpp

#include <stdio.h>
#include <string>
#include <map>
#include <memory>

#include "Socket.hpp"
#include "SocketServer.hpp"

namespace mc
{
	namespace chat
	{
		class chat_room : protected networking::socket_server
		{
		public:
			struct participant
			{
				std::string m_username;
				int m_user_id;
				networking::internet_socket m_socket;
			};
			
		private:
			using int_participant_map_t = std::map<int, std::shared_ptr<participant>>;
			
			int_participant_map_t m_participants;
			size_t m_max_size;
			std::string m_room_name;
			uint16_t m_port;
			
		public:
			chat_room();
			virtual ~chat_room() = default;
			
			void set_port(const uint16_t port);
			
			void set_max_room_size(size_t max) noexcept;
			size_t get_max_room_size() const noexcept;
			
			void set_room_name(const std::string& name) noexcept;
			const std::string& get_room_name() const noexcept;
			
			void open_room();
			void close_room();
			
		private:
			void add_participant(std::shared_ptr<participant>& p);
			void remove_participant(int id);
			
			void message_participant(int participant_id, const std::string& message);
			void message_all(const std::string& msg);
			void message_from_sender_to_all(const participant& sender, const std::string& msg);
			
			void write_socket_msg(const networking::socket& socket, const std::string& msg) const;
			
			void handle_new_participant(participant& p);
			
			void send_participant_joined_message(const participant& p);
			
			void send_welcome_message(const participant& p);
			void send_user_commands(const participant& p);
			
			void handle_set_name_command(participant& p, const std::string& command);
			void handle_list_users_command(const participant& p);
			
			bool search_for_participant_by_name(const std::string& name, int *pParticipant_id = nullptr);
		};
	}
}

#endif /* ChatRoom_hpp */
