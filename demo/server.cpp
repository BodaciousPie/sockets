//
//  server.cpp
//  Sockets
//
//  Created by Matthew Cross on 07/04/2018.
//
//

#include <stdio.h>

#include "Socket.hpp"
#include "SocketServer.hpp"
#include "ChatRoom.hpp"

//todo - interrupt handling, close sockets

int main(int argc, char *argv[])
{
	int rv = 0;
	
	try {
		mc::chat::chat_room room;
		
		room.set_room_name("Test room");
		room.set_port(8000);
		
		room.open_room();
		
	} catch (const std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		rv = -1;
	}
	
//	mc::networking::socket_server server;
//	
//	try {
//		server.bind_port(8000);
//		server.listen();
//		server.accept([](mc::networking::socket& peerSocket) {
//			
//			unsigned char eof = EOF;
//			
//			const char *pHello = "Hello world!";
//				
//			peerSocket.send_bytes(pHello, strlen(pHello) + 1);
//			peerSocket.send_bytes(&eof, sizeof(unsigned char));
//			peerSocket.close();
//		});
//		
//	} catch (std::exception& ex) {
//		std::cerr << ex.what() << std::endl;
//	}
	
	return rv;
}
