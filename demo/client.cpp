//
//  client.cpp
//  Sockets
//
//  Created by Matthew Cross on 07/04/2018.
//
//

#include <memory>
#include <stdio.h>
#include <vector>
#include <iostream>
#include <poll.h>
#include <thread>
#include <string>
#include <string.h>

#include "SocketClient.hpp"

//todo - generalise, don't read one byte at a time
ssize_t read_message(std::string& out, mc::networking::socket& socket)
{
	char c = -1;
	ssize_t rv = 0;
	
	while (true)
	{
		c = -1;
		
		ssize_t bytes_read = 0;
		
		if ((bytes_read = socket.read_bytes(&c, 1)) > 0)
		{
			rv += bytes_read;
			
			if ((c == '\0') || (c == EOF)) break;
			else out += c;
		}
		else
		{
			break;
		}
	}
	
	if (rv > 0) out += '\0';
	
	return rv;
}

void read_stdin_send_msg(mc::networking::socket& socket)
{
	std::string msg;
	
	if (std::getline(std::cin, msg).good())
	{
		socket.send_bytes(msg.c_str(), msg.length() + 1);
	}
}

struct chat_arguments
{
    std::string m_ip_address;
    uint16_t m_port = 8000;
};

int parse_args(chat_arguments& args, int argc, char *argv[])
{
    if (argc != 3) return -1;

    args.m_ip_address = argv[1];

    try {
        args.m_port = static_cast<uint16_t>(std::stoi(argv[2]));
    } catch (...) {
        return -1;
    }

    return 0;
}

void show_usage()
{
    printf("Usage: client [HOSTNAME] [PORT]\n");
}

int main(int argc, char *argv[])
{
	int rv = 0;

    chat_arguments args;

    if (parse_args(args, argc, argv) == -1)
    {
        show_usage();
        rv = -1;
    }
    else
    {
        try {
            //subclass & add chat specific functionality
            mc::networking::socket_client client;

            client.connect_to_server(args.m_ip_address, args.m_port);

            //clean this up
            std::thread([&client](){
                try {
                    while (true)
                    {
                        read_stdin_send_msg(client.get_socket());
                    }
                } catch (...) {

                }
            }).detach();

            while (true)
            {
                struct pollfd pfd;
                memset(&pfd, 0, sizeof(struct pollfd));
                pfd.fd		= client.get_socket().get_file_descriptor();
                pfd.events	= POLLIN | POLLERR | POLLHUP | POLLNVAL;

                if (poll(&pfd, 1, 6000) != -1)
                {
                    if ((pfd.revents & POLLIN) == 1)
                    {
                        std::string msg;
                        if (read_message(msg, client.get_socket()) > 0)
                        {
                            std::cout << msg;
                            std::flush(std::cout);
                        }
                        else
                        {
                            std::cerr << "Server hung up unexpectedly." << std::endl;
                            break;
                        }
                    }
                    else if (((pfd.revents & POLLERR) == 1) || ((pfd.revents & POLLHUP) == 1) || ((pfd.revents & POLLNVAL) == 1))
                    {
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
            
        } catch (std::exception& ex) {
            std::cerr << ex.what() << std::endl;
            
            rv = -1;
        }
    }

	return rv;
}
