//
//  Socket.cpp
//  SocketServer
//
//  Created by Matthew Cross on 09/03/2017.
//
//

#include "Socket.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <iostream>
#include <stdexcept>

mc::networking::socket::socket(int fd)
:
m_fd(fd)
{
	if (fd < 0)
	{
		throw std::runtime_error("Error opening socket");
	}
}

mc::networking::socket::~socket()
{
	close();
}

void mc::networking::socket::close()
{
	if (m_fd > 0)
	{
		::close(m_fd);
		m_fd = -1;
	}
}

ssize_t mc::networking::socket::send_bytes(const void *buf, size_t len) const
{
	ssize_t bytesSent = 0;
	
	if ((bytesSent = send(m_fd, buf, len, 0)) < 0)
	{
		throw std::runtime_error("Failed to send bytes");
	}
	
	return bytesSent;
}

ssize_t mc::networking::socket::read_bytes(void *buf, size_t len) const
{
	ssize_t n = recv(m_fd, buf, len, MSG_WAITALL);// read(m_socketFd, buf, len);
	
	if (n < 0)
	{
		throw std::runtime_error("Failed reading from socket");
	}
	
	return n;
}

////////

#define IP_V4 AF_INET
#define IP_V6 AF_INET6

mc::networking::internet_socket::internet_socket()
:
mc::networking::socket(::socket(IP_V4, SOCK_STREAM, 0))
{
	
}

mc::networking::internet_socket::internet_socket(int fd)
:
mc::networking::socket(fd)
{
	
}

void mc::networking::internet_socket::set_address(const struct sockaddr_in& addr)
{
	memset(&m_addr, 0, sizeof(m_addr));
	memcpy(&m_addr, &addr, sizeof(m_addr));
	
	char address[INET6_ADDRSTRLEN];
	memset(address, 0, sizeof(address));
	
	if (addr.sin_family == AF_INET)
	{
		m_port = ntohs(addr.sin_port);
		inet_ntop(AF_INET, &m_addr.sin_addr, address, sizeof(address));
		
		m_ip = address;
	}
}

struct sockaddr_in& mc::networking::internet_socket::get_address() noexcept
{
	return m_addr;
}

const std::string& mc::networking::internet_socket::get_ip_address() const noexcept
{
	return m_ip;
}

int mc::networking::internet_socket::get_port() const noexcept
{
	return m_port;
}
