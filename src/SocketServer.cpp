//
//  SocketServer.cpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#include <thread>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include "SocketServer.hpp"

mc::networking::socket_server::socket_server(std::unique_ptr<socket> upSocket)
:
m_pListeningSocket(std::move(upSocket))
{
	
}

mc::networking::socket_server::socket_server()
:
mc::networking::socket_server::socket_server(std::unique_ptr<socket>(new internet_socket()))
{
	
}

void mc::networking::socket_server::bind_port(const uint16_t port)
{
	memset(&m_serverAddress, 0, sizeof(m_serverAddress));
	
	m_serverAddress.sin_family			= AF_INET;
	m_serverAddress.sin_addr.s_addr		= INADDR_ANY;
	m_serverAddress.sin_port			= htons(port);
	
	if (::bind(m_pListeningSocket->get_file_descriptor(), reinterpret_cast<struct sockaddr *>(&m_serverAddress), sizeof(m_serverAddress)) < 0)
	{
		throw std::runtime_error("Failed to bind socket to address");
	}
}

int mc::networking::socket_server::get_bound_port() const noexcept
{
	return ntohs(m_serverAddress.sin_port);
}

void mc::networking::socket_server::listen()
{
	if (::listen(m_pListeningSocket->get_file_descriptor(), m_maxConnectionQueueLength) != 0)
	{
		throw std::runtime_error("Failed to listen on port: " + std::to_string(get_bound_port()) + " with error: \"" + strerror(errno) + "\"");
	}
	
	std::cout << "Listening for connections on port " << get_bound_port() << std::endl;
}

void ::mc::networking::socket_server::accept(const std::function<void(socket& peerSocket)>& handlerFunc)
{
    int fd = 0;

	while (true)
	{
        sleep(5);

		struct sockaddr_in cli_addr;
		
		socklen_t clilen;
		clilen = sizeof(cli_addr);

        fd = ::accept(m_pListeningSocket->get_file_descriptor(), reinterpret_cast<struct sockaddr *>(&cli_addr), &clilen);

        if (fd == -1) continue;

        std::thread([fd, &handlerFunc, cli_addr](){
            std::string ip_address;
            std::string port;
            try {
                internet_socket peerSocket(fd);
                peerSocket.set_address(cli_addr);

                ip_address = peerSocket.get_ip_address();
                port = peerSocket.get_port();

                std::cout << ip_address << ":" << port << " connected." << std::endl;

                handlerFunc(peerSocket);

            } catch (std::exception& ex) {
                std::cerr << ex.what() << std::endl;
            }
            std::cout << ip_address << ":" << port << " disconnected." << std::endl;
        }).detach();
	}
}

mc::networking::socket_server::~socket_server()
{
	m_pListeningSocket->close();
}
