//
//  SocketClient.cpp
//  SocketServer
//
//  Created by Matthew Cross on 08/03/2017.
//
//

#include "SocketClient.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>

mc::networking::socket_client::socket_client(std::unique_ptr<socket> upSocket)
:
m_upSocket(std::move(upSocket))
{

}

mc::networking::socket_client::socket_client()
:
m_upSocket(new internet_socket())
{
	
}

mc::networking::socket_client::~socket_client()
{
	m_upSocket->close();
}

void mc::networking::socket_client::connect_to_server(const std::string& host, const uint16_t port)
{
	m_pServer = gethostbyname(host.c_str());
	
	if (!m_pServer)
	{
		throw std::runtime_error("Unable to find host: " + host);
	}
	
	struct sockaddr_in serv_addr;
	memset(&serv_addr, 0, sizeof(serv_addr));
	
	serv_addr.sin_family	= AF_INET;
	serv_addr.sin_port		= htons(port);
	
	memcpy(reinterpret_cast<char *>(&serv_addr.sin_addr.s_addr), reinterpret_cast<char *>(m_pServer->h_addr), m_pServer->h_length);
	
	if (connect(m_upSocket->get_file_descriptor(), reinterpret_cast<struct sockaddr *>(&serv_addr), sizeof(serv_addr)) < 0)
	{
        throw std::runtime_error("Failed to connect to server: " + host + ":" + std::to_string(port));
	}
}
