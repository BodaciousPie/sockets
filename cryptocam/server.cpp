#include <string>
#include <iostream>
#include <assert.h>

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>  // Gaussian Blur
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O

#include "SocketServer.hpp"

using namespace cv;

struct program_arguments
{
    uint16_t port;
    const int argc = 2;
};

static void show_usage()
{
    printf("Usage: camera_server [PORT]\n");
}

static void parse_args(program_arguments& args, int argc, char *argv[])
{
    if (argc != args.argc) throw std::runtime_error("Failed to parse arguments.");

    try {
        args.port = static_cast<uint16_t>(std::stoi(argv[1]));
    } catch (...) {
        throw std::runtime_error("Failed to parse arguments.");
    }
}

static void send_mat_headers(const mc::networking::socket& socket, Mat& img)
{
	std::cout << "Channels: " << img.channels() << std::endl;
	std::cout << "Image type: " << img.type() << std::endl;
	std::cout << "Image size: " << img.size() << std::endl;
	std::cout << "Image depth: " << img.depth() << std::endl;
	std::cout << "Image step: " << img.step << std::endl;
	
//	int channels	= htons(img.channels());
	int64_t type		= htons(img.type());
	int64_t width		= htons(img.size().width);
	int64_t height		= htons(img.size().height);
	
	int64_t data[3] = {type, width, height};
	
	socket.send_bytes(data, sizeof(data));
}

int main(int argc, char *argv[])
{
    try {
        program_arguments args;

        try {
            parse_args(args, argc, argv);
        } catch (...) {
            show_usage();
            throw;
        }

        VideoCapture capture_device(0); //default camera

        if (!capture_device.isOpened())
        {
            throw std::runtime_error("Failed to open camera.");
        }

        mc::networking::socket_server server;

        server.bind_port(args.port);
        server.listen();
        server.accept([&capture_device](mc::networking::socket& peer_socket) {

            Mat header_img;
            capture_device >> header_img;

            Mat img;
            img = Mat::zeros(header_img.size(), header_img.type());

            if (!img.isContinuous())
            {
                img = img.clone();
            }

            int imgSize = img.total() * img.elemSize();

            send_mat_headers(peer_socket, header_img);

            while (true)
            {
                capture_device >> img;

                if (img.empty())
                {
                    std::cout << " < < <  Game over!  > > > ";
                    break;
                }

                peer_socket.send_bytes(img.data, imgSize);
            }
        });
    } catch (const std::exception& ex) {
        std::cerr << ex.what() << std::endl;
        return -1;
    }
	
	return 0;
}



//	Mat img;
//	std::cout << "Image type:" << img.type() << std::endl;
//	img = Mat::zeros(480, 640, CV_8UC1);
//	//make it continuous
//	std::cout << "Image type:" << img.type() << std::endl;
//	if (!img.isContinuous())
//	{
//		img = img.clone();
//		std::cout << "Image type:" << img.type() << std::endl;
//	}
//
//	int imgSize = img.total() * img.elemSize();
//	int bytes = 0;
//
//	namedWindow("CV Video Client", 1);
//
//	while (true)
//	{
//		defCam >> img;
//		std::cout << "Image type:" << img.type() << std::endl;
//
//		imshow("CV Video Client", img);
//
//		waitKey(10);
//	}
//
