#include <iostream>

#include <string>
#include <assert.h>
#include <iostream>

#include "SocketClient.hpp"

#include <opencv2/core.hpp>     // Basic OpenCV structures (cv::Mat, Scalar)
#include <opencv2/imgproc.hpp>  // Gaussian Blur
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>  // OpenCV window I/O

using namespace cv;

struct program_arguments
{
    std::string ip_address;
    uint16_t port;
    const int argc = 3;
};

static void show_usage()
{
    printf("Usage: camera_client [HOST] [PORT]\n");
}

static void parse_args(program_arguments& args, int argc, char *argv[])
{
    if (argc != args.argc) throw std::runtime_error("Failed to parse arguments.");

    args.ip_address = argv[1];

    try {
        args.port = static_cast<uint16_t>(std::stoi(argv[2]));
    } catch (...) {
        throw std::runtime_error("Failed to parse arguments.");
    }
}

typedef struct
{
	int64_t m_type = 0;
	int64_t m_width = 0;
	int64_t m_height = 0;
} mat_headers_t;

void read_mat_headers(mc::networking::socket& socket, mat_headers_t& headers)
{
	int64_t data[3];
	memset(data, 0, sizeof(data));
	
	socket.read_bytes(data, sizeof(data));
	
	int64_t type		= ntohs(data[0]);
	int64_t width		= ntohs(data[1]);
	int64_t height		= ntohs(data[2]);
	
	headers.m_type = type;
	headers.m_width = width;
	headers.m_height = height;
}

int main(int argc, char *argv[])
{
	int rv = 0;
	
	try {
        program_arguments args;

        try {
            parse_args(args, argc, argv);
        } catch (...) {
            show_usage();
            throw;
        }

        mc::networking::socket_client client;
		
		client.connect_to_server(args.ip_address, args.port);
		
		mat_headers_t headers;
		
		read_mat_headers(client.get_socket(), headers);
		
		Size sz(headers.m_width, headers.m_height);
		
		Mat img;
		img = Mat::zeros(sz, headers.m_type);
		const size_t imgSize = img.total() * img.elemSize();
		int key = 0;
		
		if (!img.isContinuous())
		{
			img = img.clone();
		}
		
		std::cout << "Image Size:" << imgSize << std::endl;
		
		namedWindow("CV Video Client");
		
		while (key != 27)
		{
			client.get_socket().read_bytes(img.data, imgSize);
			
			imshow("CV Video Client", img);
			std::cout << "Image type: " << img.type() << std::endl;
			std::cout << "Channels: " << img.channels() << std::endl;
			std::cout << "Image size: " << img.size() << std::endl;
			std::cout << "Image depth: " << img.depth() << std::endl;
			std::cout << "Image step: " << img.step << std::endl;
			
			key = waitKey(10);
		}
		
	} catch (std::exception& ex) {
		std::cerr << ex.what() << std::endl;
		
		rv = -1;
	}
	
	return rv;
}
